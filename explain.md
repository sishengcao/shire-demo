## 说明

### 使用本项目需要先创建名字为 cli 的数据库， 导入 conf/init.sql 的数据

###  默认 管理账号 admin,fongtech123

### 接口返回结果格式：{"code": 200, "message": "成功", "data": {}}
    1. code，正常返回时为200，其余数字均为异常
    2. message，请求返回结果说明
    3. data，接口需要返回数据时才返回该字段

### 分页接口请求参数格式   

```
{
  "cursor": 1,  // "页数"
   "limit": 20 //  “分页大小"
   ”ascs“： ”“   // 排序字段 可传多个
   ”descs“： “”  // 排序字段 可传多个
}

```
    
### 分页接口返回结果格式

``` json
{
  "code": 200,
  "message": "操作成功",
  "data": {
    "pageNum": 1,
    "pageSize": 20,
    "totalPage": 1,
    "total": 20,
    "list": []
  }
}
```
    1. code，正常返回时为200，其余数字均为异常
    2. message，请求返回结果说明
    3. data，接口需要返回数据时才返回该字段
    
<br>

## 接口文档

###   http://localhost:8080/doc.html

###  注意 请求 Token 头格式为： Bearer + " "  + Token

package net.fongtech.assist.web.controller.system;


import net.fongtech.assist.commom.framework.controller.SuperController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统用户角色关系表 前端控制器
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
@RestController
@RequestMapping("/userRole")
public class UserRoleRestController extends SuperController {

}


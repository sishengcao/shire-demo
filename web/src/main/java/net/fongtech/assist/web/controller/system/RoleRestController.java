package net.fongtech.assist.web.controller.system;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.fongtech.assist.biz.teacher.service.IRoleService;
import net.fongtech.assist.commom.common.annotations.Resources;
import net.fongtech.assist.commom.common.api.CommonPage;
import net.fongtech.assist.commom.common.api.CommonResult;
import net.fongtech.assist.commom.enums.AuthTypeEnum;
import net.fongtech.assist.commom.framework.controller.SuperController;
import net.fongtech.assist.commom.model.dto.RoleDTO;
import net.fongtech.assist.commom.model.entity.Role;
import net.fongtech.assist.commom.model.param.RolePARM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
@Api(tags = "角色操作", description = "角色操作")
@RestController
@RequestMapping("/role")
public class RoleRestController extends SuperController {

    @Autowired
    private IRoleService roleService;

    @Resources(auth = AuthTypeEnum.AUTH)
    @ApiOperation(value = "查询所有角色(分页)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleName", value = "需要查询的角色名", paramType = "query")
    })
    @GetMapping
    public CommonResult<CommonPage<RoleDTO>> page(@RequestParam(value = "roleName", required = false) String roleName) {
        return CommonResult.success(CommonPage.restPage( roleService.pageRoleDTO(this.<Role>getPage(), roleName)));
    }

    @Resources(auth = AuthTypeEnum.AUTH)
    @ApiOperation(value = "查询所有角色")
    @GetMapping("/roles")
    public CommonResult<List<Role>> list() {
        return CommonResult.success(roleService.list());
    }

    @Resources(auth = AuthTypeEnum.AUTH)
    @ApiOperation(value = "查询单个角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, paramType = "path")
    })
    @GetMapping("/{id}")
    public CommonResult<Role> get(@PathVariable("id") Integer id) {
        Role role = roleService.getById(id);
        return CommonResult.success(role);
    }

    @Resources(auth = AuthTypeEnum.AUTH)
    @ApiOperation(value = "添加角色")
    @PostMapping
    public CommonResult<Void> create(@RequestBody @Validated(RolePARM.Create.class) RolePARM rolePARM) {
        roleService.save(rolePARM.convert(Role.class));
        return CommonResult.success();
    }

    @Resources(auth = AuthTypeEnum.AUTH)
    @ApiOperation(value = "修改角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, paramType = "path")
    })
    @PutMapping("/{id}")
    public CommonResult<Void> update(@PathVariable("id") Integer id, @RequestBody @Validated(RolePARM.Update.class) RolePARM rolePARM) {
        Role role = rolePARM.convert(Role.class);
        role.setId(id);
        roleService.updateById(role);
        return CommonResult.success();
    }

    @Resources(auth = AuthTypeEnum.AUTH)
    @ApiOperation(value = "删除角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, paramType = "path")
    })
    @DeleteMapping("/{id}")
    public CommonResult<Void> delete(@PathVariable("id") Integer id) {
        roleService.removeById(id);
        return CommonResult.success();
    }
}


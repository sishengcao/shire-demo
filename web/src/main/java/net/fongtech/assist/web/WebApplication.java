package net.fongtech.assist.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@MapperScan("net.fongtech.assist.dao.*.**")
@ComponentScans(value = {
        @ComponentScan(value = "net.fongtech.assist.biz"),
        @ComponentScan(value = "net.fongtech.assist.commom"),
        @ComponentScan(value = "net.fongtech.assist.dao")
})
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

}

package net.fongtech.assist.web.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.fongtech.assist.biz.teacher.service.IUserService;
import net.fongtech.assist.commom.common.annotations.Resources;
import net.fongtech.assist.commom.common.api.CommonResult;
import net.fongtech.assist.commom.common.util.IpUtils;
import net.fongtech.assist.commom.enums.AuthTypeEnum;
import net.fongtech.assist.commom.framework.controller.SuperController;
import net.fongtech.assist.commom.model.dto.TokenDTO;
import net.fongtech.assist.commom.model.dto.UserDetailsDTO;
import net.fongtech.assist.commom.model.entity.User;
import net.fongtech.assist.commom.model.param.AccountInfoPARM;
import net.fongtech.assist.commom.model.param.LoginPARM;
import net.fongtech.assist.commom.model.param.PasswordPARM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 账户 前端控制器
 * </p>
 *
 * @author Caratacus
 */
@Api(tags = "账号操作", description = "账号操作")
@RestController
@RequestMapping(value = "/account", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Validated
public class AccountRestController extends SuperController {

    @Autowired
    private IUserService userService;

    @Resources
    @ApiOperation("获取Token")
    @PostMapping("/token")
    public CommonResult<TokenDTO> getToken(@RequestBody @Validated LoginPARM loginPARM) {
        User user = userService.login(loginPARM.getLoginName(), loginPARM.getPassword(), IpUtils.getIpAddr(request));
        TokenDTO tokenDTO = userService.getToken(user);
        return CommonResult.success(tokenDTO);
    }

    @Resources(auth = AuthTypeEnum.LOGIN)
    @ApiOperation("清除Token")
    @DeleteMapping("/token")
    public CommonResult<Void> removeToken() {
        return CommonResult.success();
    }

    @Resources(auth = AuthTypeEnum.LOGIN)
    @ApiOperation("修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "oldPassword", value = "原密码", required = true, dataType = "String"),
            @ApiImplicitParam(name = "newPassword", value = "新密码", required = true, dataType = "String"),
    })
    @PutMapping("/password")
    public CommonResult<Void> updatePassword(@RequestBody @Validated PasswordPARM passwordPARM) {
        userService.updatePassword(currentUid(), passwordPARM.getOldPassword(), passwordPARM.getNewPassword());
        return CommonResult.success();
    }

    @Resources(auth = AuthTypeEnum.LOGIN)
    @ApiOperation("获取账户详情")
    @GetMapping("/info")
    public CommonResult<UserDetailsDTO> accountInfo() {
        Integer uid = currentUid();
        UserDetailsDTO userDetails = userService.getUserDetails(uid);
        return CommonResult.success(userDetails);
    }

    @Resources(auth = AuthTypeEnum.LOGIN)
    @ApiOperation("修改账户信息")
    @PutMapping("/info")
    public CommonResult<Void> accountInfo(@RequestBody @Validated AccountInfoPARM accountInfoPARM) {
        Integer uid = currentUid();
        User user = accountInfoPARM.convert(User.class);
        user.setId(uid);
        userService.updateById(user);
        return CommonResult.success();
    }
}


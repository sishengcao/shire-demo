/*
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package net.fongtech.assist.web.controller.system;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.fongtech.assist.biz.teacher.service.IResourceService;
import net.fongtech.assist.biz.teacher.service.ScanMappings;
import net.fongtech.assist.commom.common.annotations.Resources;
import net.fongtech.assist.commom.common.api.CommonPage;
import net.fongtech.assist.commom.common.api.CommonResult;
import net.fongtech.assist.commom.enums.AuthTypeEnum;
import net.fongtech.assist.commom.framework.controller.SuperController;
import net.fongtech.assist.commom.model.entity.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 资源表 前端控制器
 * </p>
 *
 * @author cli
 */
@Api(tags = "资源操作", description = "资源操作")
@RestController
@RequestMapping(value = "/resource", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Validated
public class ResourceRestController extends SuperController {

        @Autowired
        private IResourceService resourceService;

        @Autowired
        private ScanMappings scanMappings;

        @Resources(auth = AuthTypeEnum.LOGIN)
        @ApiOperation(value = "查询所有资源(分页)")
        @ApiImplicitParams({
                @ApiImplicitParam(name = "resourceName", value = "需要查询的资源名", paramType = "query"),
                @ApiImplicitParam(name = "method", value = "需要查询的请求方式", paramType = "query"),
                @ApiImplicitParam(name = "authType", value = "权限认证类型", paramType = "query")
        })
        @GetMapping
        public CommonResult<CommonPage<Resource>> page(@RequestParam(value = "resourceName", required = false) String resourceName,
                                                       @RequestParam(value = "method", required = false) String method,
                                                       @RequestParam(value = "authType", required = false) AuthTypeEnum authType
        ) {
                return CommonResult.success(CommonPage.restPage(
                        resourceService.query()
                                .like(StringUtils.isNotEmpty(resourceName), Resource::getResourceName, resourceName)
                                .eq(StringUtils.isNotEmpty(method), Resource::getMethod, method)
                                .eq(Objects.nonNull(authType), Resource::getAuthType, authType)
                                .page(this.<Resource>getPage())
                        )
                );
        }

        @Resources(auth = AuthTypeEnum.AUTH)
        @ApiOperation(value = "查询所有资源")
        @GetMapping("/resources")
        public CommonResult<List<Resource>> list() {
                return CommonResult.success(resourceService.list());
        }

        @Resources(auth = AuthTypeEnum.LOGIN)
        @ApiOperation(value = "刷新资源")
        @PutMapping
        public CommonResult<Void> refresh() {
                scanMappings.doScan();
                return CommonResult.success();
        }

}

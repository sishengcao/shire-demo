/*
 Navicat Premium Data Transfer

 Source Server         : 39.100.98.131
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 39.100.98.131:3306
 Source Schema         : cli

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 20/01/2021 16:48:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resource_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源名称',
  `mapping` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '路径映射',
  `method` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求方式',
  `auth_type` smallint(2) NOT NULL COMMENT '权限认证类型',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `perm` varchar(68) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '资源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of resource
-- ----------------------------
INSERT INTO `resource` VALUES ('228887b7276edbacaed5954c97f5090a', '查询所有用户', '/user', 'GET', 3, '2019-09-11 17:01:44', 'GET:/user');
INSERT INTO `resource` VALUES ('29c4c75326ecf3a82f815c43b0085b2f', '修改账户信息', '/account/info', 'PUT', 1, '2019-09-11 17:01:44', 'PUT:/account/info');
INSERT INTO `resource` VALUES ('2fd8f53f78a2ec49f551caa88f2d676f', '查询所有角色', '/role/roles', 'GET', 3, '2019-09-11 17:01:44', 'GET:/role/roles');
INSERT INTO `resource` VALUES ('3f518eb956fe58adec52151474d52c0a', '服务器监控', '/system/server/monitor', 'GET', 1, '2020-07-25 18:26:34', 'GET:/system/server/monitor');
INSERT INTO `resource` VALUES ('3f533650ab547279bfa70e7001f9a17a', '通用上传请求', '/common/upload', 'POST', 2, '2020-07-23 17:07:22', 'POST:/common/upload');
INSERT INTO `resource` VALUES ('46a5d7a79e6ba91e6709f3361a2edd81', '添加角色', '/role', 'POST', 3, '2019-09-11 17:01:44', 'POST:/role');
INSERT INTO `resource` VALUES ('470c01a7ac3fbe02548f2abd372fff60', '查询所有角色(分页)', '/role', 'GET', 3, '2019-09-11 17:01:44', 'GET:/role');
INSERT INTO `resource` VALUES ('49764a50d0e0f74790ced2586b03bff8', '设置用户状态', '/user/{id}/status', 'PUT', 3, '2019-09-11 17:01:44', 'PUT:/user/{id}/status');
INSERT INTO `resource` VALUES ('54522abbe7d3a2a40e7edbc9339acc50', '修改用户', '/user/{id}', 'PUT', 3, '2019-09-11 17:01:44', 'PUT:/user/{id}');
INSERT INTO `resource` VALUES ('59e61b4b812fba167ba2d0b55ac61ea1', '查询所有资源', '/resource/resources', 'GET', 3, '2019-09-11 17:01:44', 'GET:/resource/resources');
INSERT INTO `resource` VALUES ('5e5b895e3d4dcef7510ea77fa6b4fb32', '查询单个角色', '/role/{id}', 'GET', 3, '2019-09-11 17:01:44', 'GET:/role/{id}');
INSERT INTO `resource` VALUES ('6126b2ab47c45309ac069198dbe3b3d2', '通用下载请求', '/common/download', 'GET', 2, '2020-07-23 17:07:22', 'GET:/common/download');
INSERT INTO `resource` VALUES ('6ab0f8a49671e489f11a1bef2fcaf102', '清除Token', '/account/token', 'DELETE', 1, '2019-09-11 17:01:44', 'DELETE:/account/token');
INSERT INTO `resource` VALUES ('6c3514540b1e2e1589a9c9566273c7c7', '查询单个用户', '/user/{id}', 'GET', 3, '2019-09-11 17:01:44', 'GET:/user/{id}');
INSERT INTO `resource` VALUES ('7025c7532d2a58d3de4dde7c66d363f3', '刷新资源', '/resource', 'PUT', 1, '2019-09-11 17:01:44', 'PUT:/resource');
INSERT INTO `resource` VALUES ('781ce3ada014ed040c788a1fa889b188', '获取验证码图片', '/captcha/captchaImage', 'GET', 2, '2020-07-25 15:49:06', 'GET:/captcha/captchaImage');
INSERT INTO `resource` VALUES ('7b9ba632c421b4fa2752f573c118cbb1', '获取验证码图片', '/system/captcha/captchaImage', 'GET', 2, '2020-07-25 18:26:34', 'GET:/system/captcha/captchaImage');
INSERT INTO `resource` VALUES ('842e33410b5a97b6c797e4782c97a90e', '获取Token', '/account/token', 'POST', 2, '2019-09-11 17:01:44', 'POST:/account/token');
INSERT INTO `resource` VALUES ('9be8e0df48c8ba879e5b079c73662ee4', '创建用户', '/user', 'POST', 3, '2019-09-11 17:01:44', 'POST:/user');
INSERT INTO `resource` VALUES ('a8bb9f9e46fe013125532c6b7747e07f', '查询所有资源(分页)', '/resource', 'GET', 1, '2019-09-11 17:01:44', 'GET:/resource');
INSERT INTO `resource` VALUES ('b7b1c22076c0d28c8d0fe4c0b09b6a6e', '修改角色', '/role/{id}', 'PUT', 3, '2019-09-11 17:01:44', 'PUT:/role/{id}');
INSERT INTO `resource` VALUES ('c2db9729dcd4a7d703e45411bb445dfd', '修改密码', '/account/password', 'PUT', 1, '2019-09-11 17:01:44', 'PUT:/account/password');
INSERT INTO `resource` VALUES ('ce100a1481da878f05b028716b811a9b', '导出资源', '/resource/export', 'GET', 1, '2020-07-24 15:11:54', 'GET:/resource/export');
INSERT INTO `resource` VALUES ('d245ace645ea40e898178ee13c24297f', '重置用户密码', '/user/{id}/password', 'PUT', 3, '2019-09-11 17:01:44', 'PUT:/user/{id}/password');
INSERT INTO `resource` VALUES ('ddf4e3fd39264da50eec54cfd02731f4', '删除角色', '/role/{id}', 'DELETE', 3, '2019-09-11 17:01:44', 'DELETE:/role/{id}');
INSERT INTO `resource` VALUES ('e78940daf86b9ac5563d539e8802429c', '获取账户详情', '/account/info', 'GET', 1, '2019-09-11 17:01:44', 'GET:/account/info');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `create_uid` int(11) NOT NULL COMMENT '创建者ID',
  `update_uid` int(11) NOT NULL COMMENT '修改者ID',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `delete_status` tinyint(2) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '超级管理员', 1, 1, '2018-11-12 15:59:43', '2018-11-12 15:59:47', '超级管理员', 0);
INSERT INTO `role` VALUES (2, '普通管理员', 1, 1, '2018-11-12 16:00:17', '2018-11-12 16:00:19', '普通管理员', 0);

-- ----------------------------
-- Table structure for role_resource
-- ----------------------------
DROP TABLE IF EXISTS `role_resource`;
CREATE TABLE `role_resource`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `resource_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色资源关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_resource
-- ----------------------------
INSERT INTO `role_resource` VALUES (2, 1, '228887b7276edbacaed5954c97f5090a');
INSERT INTO `role_resource` VALUES (3, 1, '2fd8f53f78a2ec49f551caa88f2d676f');
INSERT INTO `role_resource` VALUES (4, 1, '46a5d7a79e6ba91e6709f3361a2edd81');
INSERT INTO `role_resource` VALUES (5, 1, '470c01a7ac3fbe02548f2abd372fff60');
INSERT INTO `role_resource` VALUES (6, 1, '49764a50d0e0f74790ced2586b03bff8');
INSERT INTO `role_resource` VALUES (7, 1, '54522abbe7d3a2a40e7edbc9339acc50');
INSERT INTO `role_resource` VALUES (8, 1, '59e61b4b812fba167ba2d0b55ac61ea1');
INSERT INTO `role_resource` VALUES (9, 1, '5e5b895e3d4dcef7510ea77fa6b4fb32');
INSERT INTO `role_resource` VALUES (10, 1, '6c3514540b1e2e1589a9c9566273c7c7');
INSERT INTO `role_resource` VALUES (11, 1, '9be8e0df48c8ba879e5b079c73662ee4');
INSERT INTO `role_resource` VALUES (12, 1, 'b7b1c22076c0d28c8d0fe4c0b09b6a6e');
INSERT INTO `role_resource` VALUES (13, 1, 'd245ace645ea40e898178ee13c24297f');
INSERT INTO `role_resource` VALUES (14, 1, 'ddf4e3fd39264da50eec54cfd02731f4');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `status` smallint(2) NOT NULL COMMENT '状态 0：禁用 1：正常',
  `create_uid` int(11) NOT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `login_name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登陆名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `delete_status` tinyint(2) NULL DEFAULT 0,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'Admin', 'admin@fongtech.com', '13712345678', 0, 1, '2018-11-05 17:19:05', '2021-01-20 16:29:10', 'admin', '$apr1$admin$UXvNjSS7KAiA1bPfMEOLx.', '127.0.0.1', 0);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色ID',
  `delete_status` tinyint(2) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户角色关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (49, 1, 1, 1);
INSERT INTO `user_role` VALUES (50, 1, 1, 0);

SET FOREIGN_KEY_CHECKS = 1;

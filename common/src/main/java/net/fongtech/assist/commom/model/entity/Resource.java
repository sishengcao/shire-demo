/*
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package net.fongtech.assist.commom.model.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;

import net.fongtech.assist.commom.enums.AuthTypeEnum;
import net.fongtech.assist.commom.framework.model.convert.Convert;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * <p>
 * 资源表
 * </p>
 *
 * @author cli
 */
@TableName("resource")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Resource extends Convert {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    @ApiModelProperty(notes = "资源名称")
    private String resourceName;
    @ApiModelProperty(notes = "路径映射")
    private String mapping;
    @ApiModelProperty(notes = "请求方式")
    private String method;
    @ApiModelProperty(notes = "权限认证类型")
    private Integer authType;
    /**
     * 修改时间
     */
//    @Version 乐观锁配置
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime updateTime;
    @ApiModelProperty(notes = "权限标识")
    private String perm;

}

package net.fongtech.assist.commom.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.fongtech.assist.commom.framework.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统用户角色关系表
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@TableName("user_role")
public class UserRole extends BaseModel {

    private static final long serialVersionUID = 1L;
    public UserRole(Integer uid, Integer roleId) {
        this.uid = uid;
        this.roleId = roleId;
    }
    /**
     * 用户ID
     */
    private Integer uid;

    /**
     * 角色ID
     */
    private Integer roleId;


}

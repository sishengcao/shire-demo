package net.fongtech.assist.commom.framework.exception;

/**
 * @ClassName OperateException
 * @Date 2019/9/10 18:20
 * @Version 1.0
 */
public class OperateException  extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public OperateException(String message) {
        super(message);
    }

    public OperateException(Throwable throwable) {
        super(throwable);
    }

    public OperateException(String message, Throwable throwable) {
        super(message, throwable);
    }

}


package net.fongtech.assist.commom.framework.exception;


import net.fongtech.assist.commom.framework.model.ErrorCode;
import net.fongtech.assist.commom.framework.enums.ErrorCodeEnum;

/**
 * @ClassName BusinessException
 * @Description 业务异常类
 * @Date 2019/8/23 15:05
 * @Version 1.0
 */
public class BusinessException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private final ErrorCode errorCode;

    //自定义构造器，输入错误内容
    public BusinessException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum.msg());
        this.errorCode = errorCodeEnum.convert();
    }

    //自定义构造器，输入错误码及内容
    public BusinessException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}


package net.fongtech.assist.commom.common.api;

/**
 * 封装API的错误码
 */
public interface IErrorCode {
    int getCode();

    String getMessage();
}

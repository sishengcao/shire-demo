package net.fongtech.assist.biz.teacher.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.fongtech.assist.biz.teacher.service.IRoleService;
import net.fongtech.assist.dao.system.RoleMapper;
import net.fongtech.assist.commom.framework.service.impl.BaseServiceImpl;
import net.fongtech.assist.commom.model.dto.RoleDTO;
import net.fongtech.assist.commom.model.entity.Role;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<RoleMapper, Role> implements IRoleService {

//    @Autowired
//    private IRoleMenuService roleMenuService;

    @Override
    public IPage<RoleDTO> pageRoleDTO(Page<Role> page, String roleName) {
        IPage<Role> rolePage = query().like(StringUtils.isNotEmpty(roleName), Role::getRoleName, roleName).page(page);
        return rolePage.convert(role -> {
            RoleDTO roleDTO = role.convert(RoleDTO.class);
//            roleDTO.setMenuIds(roleMenuService.query()
//                    .select(RoleMenu::getMenuId)
//                    .eq(RoleMenu::getRoleId, role.getId())
//                    .listObjs(TypeUtils::castToInt)
//            );
            return roleDTO;
        });
    }
}

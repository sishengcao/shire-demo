package net.fongtech.assist.biz.teacher.service.impl;

import net.fongtech.assist.biz.teacher.service.IUserRoleService;
import net.fongtech.assist.dao.system.UserRoleMapper;
import net.fongtech.assist.commom.framework.service.impl.BaseServiceImpl;
import net.fongtech.assist.commom.model.entity.UserRole;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户角色关系表 服务实现类
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}

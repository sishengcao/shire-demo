package net.fongtech.assist.biz.teacher.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.fongtech.assist.commom.framework.service.BaseService;
import net.fongtech.assist.commom.model.dto.RoleDTO;
import net.fongtech.assist.commom.model.entity.Role;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
public interface IRoleService extends BaseService<Role> {
    /**
     * 获取角色详情列表
     *
     * @param page
     * @param roleName
     * @return
     */
    IPage<RoleDTO> pageRoleDTO(Page<Role> page, String roleName);
}

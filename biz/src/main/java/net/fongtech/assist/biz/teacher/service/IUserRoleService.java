package net.fongtech.assist.biz.teacher.service;


import net.fongtech.assist.commom.framework.service.BaseService;
import net.fongtech.assist.commom.model.entity.UserRole;

/**
 * <p>
 * 系统用户角色关系表 服务类
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
public interface IUserRoleService extends BaseService<UserRole> {
}

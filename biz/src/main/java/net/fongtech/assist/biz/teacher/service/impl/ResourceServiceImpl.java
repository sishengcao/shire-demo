/*
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package net.fongtech.assist.biz.teacher.service.impl;

import com.baomidou.mybatisplus.core.enums.SqlMethod;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import net.fongtech.assist.biz.teacher.service.IResourceService;
import net.fongtech.assist.dao.system.ResourceMapper;
import net.fongtech.assist.commom.enums.AuthTypeEnum;
import net.fongtech.assist.commom.framework.service.impl.BaseServiceImpl;
import net.fongtech.assist.commom.model.dto.ResourcePermDTO;
import net.fongtech.assist.commom.model.entity.Resource;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 资源表 服务实现类
 * </p>
 *
 * @author cli
 */
@Service
public class ResourceServiceImpl extends BaseServiceImpl<ResourceMapper, Resource> implements IResourceService {

    @Override
    public List<String> getUserPerms(Integer uid) {
        return getUserResourcePerms(uid).stream().map(e -> this.getResourcePermTag(e.getMethod(), e.getMapping())).collect(Collectors.toList());
    }

    @Override
    public String getResourcePermTag(String method, String mapping) {
        return method + ":" + mapping;
    }

    @Override
    public Set<ResourcePermDTO> getUserResourcePerms(Integer uid) {
        List<ResourcePermDTO> perms = getPerms(AuthTypeEnum.OPEN, AuthTypeEnum.LOGIN);
        List<ResourcePermDTO> resourcePerms = baseMapper.getUserResourcePerms(uid);
        perms.addAll(resourcePerms);
        return new HashSet<>(perms);
    }


    @Override
    @Transactional
    public boolean saveOrUpdateBatch(Collection<Resource> entityList) {
        //批量对象插入 不存在直接返回true
        if (CollectionUtils.isEmpty(entityList)) {
            return true;
        }
        Map<String, Resource> resourceMap = list2Map(Resource::getId);
        int i = 0;
        try (SqlSession batchSqlSession = sqlSessionBatch()) {
            for (Resource entity : entityList) {
                String idVal = entity.getId();
                Resource resource = resourceMap.get(idVal);
                if (Objects.nonNull(resource)) {
                    entity.setUpdateTime(resource.getUpdateTime());
                    MapperMethod.ParamMap<Resource> param = new MapperMethod.ParamMap<>();
                    param.put(Constants.ENTITY, entity);
                    batchSqlSession.update(sqlStatement(SqlMethod.UPDATE_BY_ID), param);
                } else {
                    batchSqlSession.insert(sqlStatement(SqlMethod.INSERT_ONE), entity);
                }
                if (i >= 1 && i % batchSize == 0) {
                    batchSqlSession.flushStatements();
                }
                i++;
            }
            batchSqlSession.flushStatements();
        }
        return true;
    }

    @Override
    public List<ResourcePermDTO> getOpenPerms() {
        return getPerms(AuthTypeEnum.OPEN);
    }

    @Override
    public List<ResourcePermDTO> getLoginPerms() {
        return getPerms(AuthTypeEnum.LOGIN);
    }

    @Override
    public List<ResourcePermDTO> getPerms(AuthTypeEnum... authTypes) {
        List<Integer> types = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(authTypes)){
            for (AuthTypeEnum at : authTypes){
                types.add(at.getValue());
            }
        }
        return query()
            .select(Resource::getMethod, Resource::getMapping)
            .in(ArrayUtils.isNotEmpty(authTypes), Resource::getAuthType, types)
            .entitys(e -> e.convert(ResourcePermDTO.class));
    }

    @Override
    public List<ResourcePermDTO> getPerms() {
        return getPerms((AuthTypeEnum[]) null);
    }

    @Override
    public List<ResourcePermDTO> getResourcePerms(String method) {
        return query().select(Resource::getMethod, Resource::getMapping).eq(Resource::getMethod, method).entitys(e -> e.convert(ResourcePermDTO.class));
    }


}

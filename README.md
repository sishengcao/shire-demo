# hekeda-website-api


## 前言

`hekeda-website-api`和科达官网项目。

### 组织结构

``` lua
hekeda-website-api
├── common -- 工具类及通用代码
├── biz -- 业务实现层代码
├── dao -- 持久层代码,主要对数据库操作代码
├── web -- 控制层,api入口,日志/权限拦截
```

### 技术选型

#### 后端技术

技术 | 说明 | 官网
----|----|----
Spring Boot | 容器+MVC框架 | [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
Shiro | 认证和授权框架 | [http://shiro.apache.org/](http://shiro.apache.org/)
MyBatis | ORM框架  | [http://www.mybatis.org/mybatis-3/zh/index.html](http://www.mybatis.org/mybatis-3/zh/index.html)
MyBatis-Plus| ORM框架  | [https://mybatis.plus/guide/](https://mybatis.plus/guide/)
MyBatisGenerator | 数据层代码生成 | [http://www.mybatis.org/generator/index.html](http://www.mybatis.org/generator/index.html)
PageHelper | MyBatis物理分页插件 | [http://git.oschina.net/free/Mybatis_PageHelper](http://git.oschina.net/free/Mybatis_PageHelper)
Swagger-UI | 文档生产工具 | [https://github.com/swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui)
Hibernator-Validator | 验证框架 | [http://hibernate.org/validator/](http://hibernate.org/validator/)
MYSQL | Sql数据库 | [https://www.mysql.com/](https://www.mysql.com/)
hikaricp | 数据库连接池 | [https://github.com/brettwooldridge/HikariCP](https://github.com/brettwooldridge/HikariCP)
JWT | JWT登录支持 | [https://github.com/jwtk/jjwt](https://github.com/jwtk/jjwt)
Lombok | 简化对象封装工具 | [https://github.com/rzwitserloot/lombok](https://github.com/rzwitserloot/lombok)

#### Mybatis 与 MybatisPlus 与 Mapper 技术选型

##### MybatisPlus 特性
MybatisPlus特性[参考这里](http://mp.baomidou.com/#/)
> * 无侵入：Mybatis-Plus 在 Mybatis 的基础上进行扩展，只做增强不做改变，引入 Mybatis-Plus 不会对您现有的 Mybatis 构架产生任何影响，而且 MP 支持所有 Mybatis 原生的特性
> * 依赖少：仅仅依赖 Mybatis 以及 Mybatis-Spring
> * 损耗小：启动即会自动注入基本CURD，性能基本无损耗，直接面向对象操作
> * 预防Sql注入：内置Sql注入剥离器，有效预防Sql注入攻击
> * 通用CRUD操作：内置通用 Mapper、通用 Service，仅仅通过少量配置即可实现单表大部分 CRUD 操作，更有强大的条件构造器，满足各类使用需求
> * 多种主键策略：支持多达4种主键策略（内含分布式唯一ID生成器），可自由配置，完美解决主键问题
> * 支持热加载：Mapper 对应的 XML 支持热加载，对于简单的 CRUD 操作，甚至可以无 XML 启动
> * 支持ActiveRecord：支持 ActiveRecord 形式调用，实体类只需继承 Model 类即可实现基本 CRUD 操作
> * 支持代码生成：采用代码或者 Maven 插件可快速生成 Mapper 、 Model 、 Service 、 Controller 层代码，支持模板引擎，更有超多自定义配置等您来使用（P.S. 比 Mybatis 官方的 Generator 更加强大！）
> * 支持自定义全局通用操作：支持全局通用方法注入（ Write once, use anywhere ）
> * 支持关键词自动转义：支持数据库关键词（order、key......）自动转义，还可自定义关键词
> * 内置分页插件：基于Mybatis物理分页，开发者无需关心具体操作，配置好插件之后，写分页等同于写基本List查询
> * 内置性能分析插件：可输出Sql语句以及其执行时间，建议开发测试时启用该功能，能有效解决慢查询
> * 内置全局拦截插件：提供全表 delete 、 update 操作智能分析阻断，预防误操作

##### Mapper 特性
Mapper特性[参考这里](http://git.oschina.net/free/Mapper)
* 通用CRUD操作
* 支持多数据库
* **spring-boot整合教程**
* 分页插件
* 代码生成
* **订制公共mapper接口**
* 通用的查询封装
* **对动态表的支持（即表的垂直切分，如user被划分为user_1,user_2,user_3这三张表）**
* 默认的下划线转驼峰配置

##### Mybatis 与 MybatisPlus 与 Mapper 比较
* Mybatis 拥有 Mybatis-generate 插件，可以直接从数据库生成 DAO 类以及基本的 example 进行 CURD 操作，需要注意的点是：
* Mybatis 生成的 Mapper 除了可以主键查询之外其他的都需要新建 example ，在 example 中添加对应的筛选条件才能进行查询（每个查询新建 example 和添加条件比较麻烦，后期看）。
* 在一次项目中避免多次执行 Mybatis-generator，即应当尽量在数据库表建立完整并且确定不会修改之后执行 Mybatis-generator，否则再次执行会覆盖原本的Model、DAO和映射文件的文件夹（踩过的坑，可以通过配置文件来解决）
* 缺少一些通用的查询方法，比如获取单个对象，获取所有对象等通用方法。缺少 and ( or ) 连接方法，查询很不方便。
* Mapper 在 Mybatis 的基础上增加了一些通用 CURD 操作，支持通用的查询分装，支持单个对象已经多个对象的获取，相当于 Mybatis 的拓展。
* 遗憾的是 Mapper 对一些复杂查询依旧使用 example ，查询起来也不是很方便，所以有了下面用的 Mybatis Plus。
* MybatisPlus的创造理念：在 Mybatis 的基础上只做增强不做改变，这样同样保持了MybatisPlus的轻量简洁（无第三方依赖引入包）；而Mapper的创造理念是借鉴了的hibernate的思想，有些臃肿。
* 在原生mybatis的基础上，Mapper封装更多，程序逻辑也会更为复杂；而当前MybatisPlus的代码量不大，代码结构也更为清晰；如果我们需要实现一些自身特定需求，改造起来也会更为方便简单。此外，随着MybatisPlus不断完善，我们对改组件的掌控度也会更高。
* MybatisPlus只抽象出了一个公共maper接口，该接口包含了所有的公共方法；而Mapper对于某个方法都抽象出了一个mapper接口，这种方式可以根据自己的需求去继承自己所需的公共接口，可减少mapper层的复杂度
* 代码生成，MybatisPlus提供了两种方式main方式和maven插件，其中maven插件生成方式不可用，后续会完善；而Mapper使用maven插件生成，以覆盖和全量方式生成，只生成实体与mapper；代码生成工具两者都不可取，需要自行编写。
* Mybatis Plus 支持多种复杂查询，Mybatis 不含有的 and ( or ) 连接方法，Mybatis Plus 支持。

##### 总结
综上，我觉得**MybatisPlus**更符合我们现在的需求，轻量，优雅，高效。

#### Json 转换工具

##### FastJson 和  JackSon 对比
* FastJson 号称最快的 JSON 解析库，在 JSON 解析上使用了一些特别的加速手段，比如在 JSON 解析成大对象时，对针对这种 JSON 格式生产对应的 Class 解析对象，这样会造成内存过多问题。
* 内存占用过多。Fastjson为了性能，在ThreadLocal中缓存了char[] buffer，这样避免分配内存和gc的开销。但是如果碰到了大的json(比如10M这样的)，就会占用大量的内存，而且以后都是处理小json了内存占用也回不来。
* 碰到在128~255 的字符直接异常，这些主要是西欧语言的字符，因为他用一个数组来记录 转义后的字符表示，但是数组长度只有128...
* 对有下划线的变量支持不是很方便，可能会出现序列化属性丢失的情况，这在使用第三方的接口的时候是很麻烦的，因为无法要求三方格式符合要求。
* FastJson 操作方便，JSON 工具类即可处理 JSON。
* JackSon 在 SpringBoot 中是默认引用的，不需要额外的引入 jar 包。
* JackSon 拥有超强的拓展性，能够满足许多种情况。
* 例如 null的处理 有的需要输出null，希望保留字段。有的需要不输出，输出结果里头剔除空值。
* 空字符串的处理 有的需要输出null，有的需要不输出，有的需要输出 “”
* bool的处理有的需要输出0／1，有的需要输出true／false
* 自定义对象的处理DateTime、Long、Timestamp之间的各种转换
* 字段缺失的处理有的需要报错，有的需要忽略。


### 开发环境

工具 | 版本号 | 下载
----|----|----
JDK | 1.8 | https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
Mysql | 5.7 | https://www.mysql.com/
Redis | 3.2 | https://redis.io/download
nginx | 1.10 | http://nginx.org/en/download.html

## 参考资料

- [Spring实战（第4版）](https://book.douban.com/subject/26767354/)
- [Spring Boot实战](https://book.douban.com/subject/26857423/)
- [Spring Cloud微服务实战](https://book.douban.com/subject/27025912/)
- [Spring Cloud与Docker微服务架构实战](https://book.douban.com/subject/27028228/)
- [Spring Data实战](https://book.douban.com/subject/25975186/)
- [MyBatis从入门到精通](https://book.douban.com/subject/27074809/)
- [深入浅出MySQL](https://book.douban.com/subject/25817684/)
- [循序渐进Linux（第2版）](https://book.douban.com/subject/26758194/)
- [Elasticsearch 权威指南](https://www.elastic.co/guide/cn/elasticsearch/guide/current/index.html)
- [Elasticsearch 技术解析与实战](https://book.douban.com/subject/26967826/)
- [MongoDB实战(第二版)](https://book.douban.com/subject/27061123/)
- [Kubernetes权威指南](https://book.douban.com/subject/26902153/)
- [Pro Git](https://git-scm.com/book/zh/v2)
- [mall](https://github.com/macrozheng/mall)
- [litemall](https://github.com/linlinjava/litemall)

## Feature

1. 标准的Restful风格，完美的标准化API。
2. 采用 ModelMapper 进行各种类型一键转换，数据库列字段一次性转换，无须再写 convert 函数。
3. 完美的接口日志详情打印，响应时间，请求参数，响应参数一应俱全。
4. 采用 P6spy打印 SQL ，SQL 查询一目了然。
5. 采用 Log4j2 异步打印日志，让你的日志输出不再是瓶颈。
6. 采用 Shiro 进行用户鉴权，自定义注解声明访问权限，再不用手动配置拦截路径和标注角色。
7. 采用 BootStarp API的 swagger 文档，文档跟随代码直接更新，页面上直接 debugger。
8. 采用 Mybatis Plus 进行数据库查询，直接传 condition 条件判断是否需要添加过滤，再不用写大堆 if 代码了。
9. 多条件 lambda 连接查询，各种筛选查询条件一目了然。
10. 支持 select lambda 配置，想查询什么数据即可查询什么数据。
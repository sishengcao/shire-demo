package net.fongtech.assist.dao.system;

import net.fongtech.assist.commom.framework.mapper.BaseMapper;
import net.fongtech.assist.commom.model.entity.Role;
import org.apache.ibatis.annotations.Mapper;


/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}

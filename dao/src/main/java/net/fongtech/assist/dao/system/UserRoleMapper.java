package net.fongtech.assist.dao.system;


import net.fongtech.assist.commom.framework.mapper.BaseMapper;
import net.fongtech.assist.commom.model.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统用户角色关系表 Mapper 接口
 * </p>
 *
 * @author cli
 * @since 2019-09-10
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

}

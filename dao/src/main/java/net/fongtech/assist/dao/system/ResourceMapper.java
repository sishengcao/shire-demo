/*
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package net.fongtech.assist.dao.system;

import net.fongtech.assist.commom.framework.mapper.BaseMapper;
import net.fongtech.assist.commom.model.dto.ResourcePermDTO;
import net.fongtech.assist.commom.model.entity.Resource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 资源表 Mapper 接口
 * </p>
 *
 * @author cli
 */
@Mapper
public interface ResourceMapper extends BaseMapper<Resource> {
        /**
         * 获取用户权限
         *
         * @param uid
         * @return
         */
        @Select("SELECT sr.mapping, sr.method FROM resource sr LEFT JOIN role_resource srr ON srr.resource_id = sr.id\n" +
                "        LEFT JOIN user_role sur ON sur.role_id = srr.role_id WHERE sur.uid = #{uid} AND sr.auth_type = 3")
        List<ResourcePermDTO> getUserResourcePerms(@Param("uid") Integer uid);

}
